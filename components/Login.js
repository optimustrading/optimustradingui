import React, { useState } from 'react'
import AsyncLocalStorage from '@createnextapp/async-local-storage'
import { useRecoilState } from 'recoil'
import { user } from '../atoms/user'

import Progress from './Progress'

export default function Login () {
  const [type, setType] = useState('login')
  const [passwordShown, setPasswordShown] = useState(false)
  const [curstep, setCurstep] = useState(1)
  const [userx, setUserx] = useRecoilState(user)
  const setLogin = () => {
    setType('login')
    setCurstep(0)
  }

  const handleLogin = async event => {
    event.preventDefault()
    var myHeaders = new Headers()
    myHeaders.append('Content-Type', 'application/json')
    //  email: 'papa_joo@gmail.com',
    // password: '1qaz!QAZ'
    var raw = JSON.stringify({
      email: event.target.elements.email.value,
      password: event.target.elements.password.value
    })

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    }
    fetch(`${process.env.NEXT_PUBLIC_API_URL}api/users/login`, requestOptions)
      .then(response => response.text())
      .then(async result => {
        await AsyncLocalStorage.setItem('user', result)
        setUserx(JSON.parse(result))
      })
      .catch(error => console.log('error', error))
  }
  const handleRegister = async event => {
    event.preventDefault()
    const dummy = {
      email: 'papa_joo@gmail.com',
      password: '1qaz!QAZ',
      firstName: 'Papa',
      lastName: 'Joo',
      address: 'test address',
      age: 21,
      gender: 1,
      hearAboutUsSource: 2
    }
  }
  const handleSteps = () => {
    setCurstep(curstep + 1)
  }
  return (
    <div className='grid grid-cols-1 sm:grid-cols-2'>
      <div className=' block sm:hidden'>
        <img src='/images/bglogin.png' className='w-full' />
      </div>
      <div className='layout text-white flex flex-col justify-center px-10'>
        {type === 'login' && (
          <>
            <h1 className='text-3xl pb-10 font-bold'>Log in</h1>
            <form
              className='flex flex-col justify-center'
              onSubmit={handleLogin}
            >
              <label className='pt-2 pb-2'>Email</label>
              <input
                className='w-full p-2 border border-formborder bg-transparent'
                type='email'
                placeholder='Email'
                name='email'
              />
              <div className='grid grid-cols-2 gap-4 pt-5'>
                <label className=' pt-2 pb-2'>Enter password</label>
                <div className='text-right'>
                  <img
                    src='/images/eye.png'
                    alt='eye'
                    className='float-right pt-3 cursor-pointer'
                    name='password'
                    onClick={() => setPasswordShown(!passwordShown)}
                  />
                </div>
              </div>

              <input
                className='w-full p-2 border  border-formborder bg-transparent'
                type={passwordShown ? 'text' : 'password'}
                placeholder='Password'
                name='password'
              />

              <button
                className='mt-20 w-full p-2 bg-button  text-white'
                type='submit'
              >
                Log in
              </button>
              <div className='flex justify-center mt-10'>
                Do not have an account?
                <a
                  onClick={() => setType('register')}
                  className='font-bold pl-1 cursor-pointer'
                >
                  Register now.
                </a>
              </div>
            </form>
          </>
        )}
        {type === 'register' && (
          <>
            <h1 className='text-3xl pb-10 font-bold'>Register</h1>
            <Progress curstep={curstep} />

            <form
              className='flex flex-col justify-center'
              onSubmit={handleRegister}
            >
              {curstep === 1 && (
                <>
                  <label className='pt-2 pb-2'>Email</label>
                  <input
                    className='w-full p-2 border border-formborder bg-transparent'
                    type='email'
                    placeholder='Email'
                    name='email'
                  />
                  <label className='pt-2 pb-2'>Enter password</label>
                  <input
                    className='w-full p-2 border border-formborder bg-transparent'
                    type='password'
                    placeholder='Password'
                    name='password'
                  />
                  <div className='text-xs'>
                    <div className='grid grid-cols-2'>
                      <ul className='list-disc text-gray-500'>
                        <li>At least 8 characters</li>
                        <li>At least one number</li>
                        <li>At least one uppercase letter</li>
                      </ul>
                      <ul className='list-disc text-gray-500'>
                        <li>At least one lowercase letter</li>
                        <li>At least one special character</li>
                      </ul>
                    </div>
                  </div>
                  <label className='pt-2 pb-2'>Confirm password</label>
                  <input
                    className='w-full p-2 border border-formborder bg-transparent'
                    type='password'
                    placeholder='Confirm password'
                    name='password'
                  />
                  <button
                    className='mt-20 w-full p-2 bg-button  text-white'
                    onClick={() => handleSteps()}
                  >
                    Continue
                  </button>
                </>
              )}
            </form>
          </>
        )}

        {curstep === 2 && (
          <>
            <label className='pt-2 pb-2'>Full name</label>
            <input
              className='w-full p-2 border border-formborder bg-transparent'
              type='text'
              placeholder='Full name'
              name='fullname'
            />

            <label className='pt-2 pb-2'>Address</label>
            <input
              className='w-full p-2 border border-formborder bg-transparent'
              type='text'
              placeholder='Full name'
              name='address'
            />

            <label className='pt-2 pb-2'>Age</label>
            <input
              className='w-full p-2 border border-formborder bg-transparent'
              type='number'
              placeholder='Full name'
              name='age'
            />
            <div className='pt-3'>
              <label className='pt-2 pb-2'>Gender</label>
              <button
                id='dropdownButton'
                data-dropdown-toggle='dropdown'
                className='w-full p-2 border border-formborder bg-transparent'
                type='button'
              >
                <svg
                  className='ml-2 w-4 h-4'
                  fill='none'
                  stroke='currentColor'
                  viewBox='0 0 24 24'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <path
                    stroke-linecap='round'
                    stroke-linejoin='round'
                    stroke-width='2'
                    d='M19 9l-7 7-7-7'
                  ></path>
                </svg>
              </button>

              <div
                id='dropdown'
                className='hidden z-10 w-44 text-base list-none bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700'
              >
                <ul className='py-1' aria-labelledby='dropdownButton'>
                  <li>
                    <a
                      href='#'
                      className='block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white'
                    >
                      Dashboard
                    </a>
                  </li>
                  <li>
                    <a
                      href='#'
                      className='block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white'
                    >
                      Settings
                    </a>
                  </li>
                  <li>
                    <a
                      href='#'
                      className='block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white'
                    >
                      Earnings
                    </a>
                  </li>
                  <li>
                    <a
                      href='#'
                      className='block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white'
                    >
                      Sign out
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <button
              className='mt-20 w-full p-2 bg-button  text-white'
              onClick={() => handleSteps()}
            >
              Continue
            </button>
          </>
        )}
        {curstep === 3 && (
          <>
            <button className='mt-20 w-full p-2 bg-button  text-white'>
              Continue
            </button>
          </>
        )}
      </div>

      <div className='hidden sm:block'>
        <img src='/images/bglogin.png' className='w-full' />
      </div>
    </div>
  )
}

//
