import React from 'react'

const Progress = ({ curstep }) => {
  const steps = [
    { name: 'Mandatory', id: 1, class: 'step' },
    { name: 'Optionary ', id: 2, class: 'step' },
    { name: 'Confirmation ', id: 3, class: 'step' }
  ]
  const progress = steps.map(step =>
    curstep >= step.id ? { ...step, class: 'step step-primary' } : step
  )

  return (
    <ul className='steps'>
      {progress.map(step => (
        <li key={step.id} className={step.class}>
          {step.name}
        </li>
      ))}
    </ul>
  )
}

export default Progress
