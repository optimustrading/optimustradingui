FROM node:alpine

#Creates directories
RUN mkdir -p /usr/src/app

#Sets an environment variable
ENV PORT 3000
WORKDIR /usr/src/app
COPY package.json /usr/src/app
COPY package-lock.json /usr/src/app
ENV NODE_OPTIONS="--openssl-legacy-provider"
RUN npm install
##Copy new files or directories into the filesystem of the container
COPY . /usr/src/app

#Execute commands in a new layer on top of the current image and commit the results
RUN npm run dev

#Informs container runtime that the container listens on the specified network ports at runtime
EXPOSE 8080
 