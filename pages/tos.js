import React, { useState, useEffect } from 'react'
import AsyncLocalStorage from '@createnextapp/async-local-storage'
import Layout from '../Layout/Main'

export default function Main () {
  const [user, setUser] = useState({})
  useEffect(() => {
    async function checkUser () {
      const prevdata = await AsyncLocalStorage.getItem('user')
      setUser(JSON.parse(prevdata))
    }
    checkUser()
  }, [])
  return (
    <Layout
      title='Terms and Conditions'
      active='Terms and Conditions'
      header='Terms and Conditions'
      user={user}
    >
      Tos
    </Layout>
  )
}

//<div className='layout text-white flex min-h-screen flex-col items-center justify-center text-center'></div>
