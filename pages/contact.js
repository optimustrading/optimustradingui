import React, { useState, useEffect } from 'react'
import AsyncLocalStorage from '@createnextapp/async-local-storage'
import Layout from '../Layout/Main'

export default function Main () {
  const [user, setUser] = useState({})
  useEffect(() => {
    async function checkUser () {
      const prevdata = await AsyncLocalStorage.getItem('user')
      setUser(JSON.parse(prevdata))
    }
    checkUser()
  }, [])
  return (
    <Layout title='Contact Us' active='Contact Us' header='Contact' user={user}>
      Contact
    </Layout>
  )
}

//<div className='layout text-white flex min-h-screen flex-col items-center justify-center text-center'></div>
