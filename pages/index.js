import React, { useEffect } from 'react'
import { withRouter, useRouter } from 'next/router'

import Layout from '../Layout/Main'
import AsyncLocalStorage from '@createnextapp/async-local-storage'
import { useRecoilState } from 'recoil'
import { user } from '../atoms/user'
import Login from '../components/Login'
import Panel from '../components/Panel'

const Main = ({ router }) => {
  const [userx, setUserx] = useRecoilState(user)
  const routerq = useRouter()
  console.log(routerq.query.logout)
  useEffect(() => {
    console.log('executes')
    async function checkUser () {
      if (routerq.query.logout) {
        localStorage.removeItem('user')
        setUserx(null)
      } else {
        const prevdata = await AsyncLocalStorage.getItem('user')
        setUserx(JSON.parse(prevdata))
      }
    }
    checkUser()
  }, [])
  return (
    <Layout
      title='Welcome to Optimus Trading'
      active={userx ? 'Widgets' : 'Log in/Registration'}
      header='Log in/Registration'
      user={userx}
    >
      {userx ? <Panel user={userx} /> : <Login />}
    </Layout>
  )
}
export default withRouter(Main)
