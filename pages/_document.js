import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps (ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render () {
    return (
      <Html className='scroll-smooth' style={{ scrollBehavior: 'smooth' }}>
        <Head>
          <link rel='preconnect' href='https://fonts.googleapis.com' />
          <link
            rel='preconnect'
            href='https://fonts.gstatic.com'
            crossOrigin='true'
          />
          <link
            href='https://fonts.googleapis.com/css2?family=Montserrat:wght@500;700&display=swap'
            rel='stylesheet'
          />

          <script src='https://unpkg.com/flowbite@1.3.4/dist/flowbite.js'></script>
        </Head>
        <body className='bg-bg text-white text-xs md:text-base h-screen'>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
