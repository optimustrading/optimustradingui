import React from 'react'
import Meta from '../pages/_meta'
import Navbar from '../components/NavBar'
const Layout = ({ children, title, active, user }) => {
  return (
    <>
      <Meta title={title} />
      <Navbar active={active} user={user} />
      {children}
    </>
  )
}

export default Layout
