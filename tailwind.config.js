module.exports = {
  mode: 'jit',
  plugins: [require('flowbite/plugin'), require('daisyui')],
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      header: '#16181A',
      bg: '#1C1C3A',
      formborder: '#546EE5',
      button: '#546EE5'
    }
  },
  variants: {
    extend: {}
  }
}
